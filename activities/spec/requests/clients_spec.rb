require 'rails_helper'

RSpec.describe "Clients", type: :request do
  let(:user) { create(:user) }
  let!(:clients) { create_list(:client, 5) }
  let(:client_id) { clients.first.id }
  
  describe "GET /clients" do
    context "Authentificated" do
      before { 
        login_with_user(user)
        get '/clients', headers: {
          'Authorization': response.headers['Authorization']
        }
      }
      
      it 'returns clients' do      
        expect(json).not_to be_empty
        expect(json.size).to eq(5)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
    
    context "Not Authentificated" do
      before { get '/clients' }

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  describe "GET /clients/:id" do
    before {
      login_with_user(user)
      get "/clients/#{client_id}", headers: {
        'Authorization': response.headers['Authorization']
      }
    }
    
    context 'when the record exists' do
      it 'returns client' do
        expect(json).not_to be_empty
        expect(json["id"]).to eq(client_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:client_id) { Faker::Number.number(digits: 10) }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Client/)
      end
    end
  end

  describe "Post /clients" do
    # valid payload
    let(:attributes) { { client: { name: Faker::Lorem.word } } }

    before {
      login_with_user(user)
      post '/clients', params: attributes, headers: {
        'Authorization': response.headers['Authorization']
      }
    }
    
    context 'when the request is valid' do
      it 'creates a client' do
        expect(json).not_to be_empty
        expect(json["name"]).to eq(attributes[:client][:name])
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the name is blank' do
      let(:attributes) { { client: { name: "" } } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a not valid message' do
        expect(response.body)
          .to match(/Validation failed: Name can't be blank/)
      end
    end    
  end

  describe "Put /clients/:id" do
    # valid payload
    let(:attributes) { { client: { name: Faker::Lorem.word } } }

    before {
      login_with_user(user)
      put "/clients/#{client_id}", params: attributes, headers: {
        'Authorization': response.headers['Authorization']
      }
    }
    
    context 'when the request is valid and record exist' do
      it 'updates a client' do
        expect(json).not_to be_empty
        expect(json["name"]).to eq(attributes[:client][:name])
      end

      it 'returns status code 202' do
        expect(response).to have_http_status(202)
      end
    end

    context 'when the request is invalid' do
      let(:attributes) { { client: { name: "" } } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a not valid message' do
        expect(response.body)
          .to match(/Validation failed: Name can't be blank/)
      end
    end

    context 'when the record is not found' do
      let(:client_id) { Faker::Number.number(digits: 10) }
      
      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Client/)
      end
    end
  end

  describe "Delete /clients/:id" do
    before {
      login_with_user(user)
      delete "/clients/#{client_id}", headers: {
        'Authorization': response.headers['Authorization']
      }
    }
    
    context 'when the record exist' do
      it 'deletes a client' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
    
    context 'when the record is not found' do
      let(:client_id) { Faker::Number.number(digits: 10) }
      
      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Client/)
      end
    end
  end
end
