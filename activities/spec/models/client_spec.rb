require 'rails_helper'

RSpec.describe Client, type: :model do
  # Association test
  # ensure Client model has a 1:n relationship with the TimeLog model
  it { should have_many(:time_logs).dependent(:destroy) }
  # Validation tests
  # ensure columns name is present before saving
  it { should validate_presence_of(:name) }
end
