require 'rails_helper'

RSpec.describe User, type: :model do
  it "can use Factory" do
    users = create_list(:user, 5)
    expect(users.length).to eq(5)
  end
end
