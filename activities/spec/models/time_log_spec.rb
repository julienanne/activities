require 'rails_helper'

RSpec.describe TimeLog, type: :model do
  # Association test
  # ensure an TimeLog record belongs to a single Client record
  it { should belong_to(:client) }
  # Validation test
  # ensure column description and finished_at are present before saving
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:finished_at) }
end
