class CreateTimeLogs < ActiveRecord::Migration[6.1]
  def change
    create_table :time_logs do |t|
      t.references :client, null: false, foreign_key: true
      t.text :description
      t.datetime :finished_at

      t.timestamps
    end
  end
end
