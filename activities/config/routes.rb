Rails.application.routes.draw do
  devise_for :users, skip: :sessions
  devise_scope :user do
    post 'users/sign_in' => 'devise/sessions#create', defaults: { format: :json }, :as => 'sign_in_user_session'
    delete 'users/sign_out' => 'devise/sessions#destroy', defaults: { format: :json }, :as => 'sign_out_user_session'
  end
  resources :clients, defaults: { format: :json } do
    # resources :time_logs
  end
end
