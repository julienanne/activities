class TimeLog < ApplicationRecord
  belongs_to :client

  validates_presence_of :description, :finished_at
end
