class Client < ApplicationRecord
  has_many :time_logs, dependent: :destroy

  validates_presence_of :name
end
