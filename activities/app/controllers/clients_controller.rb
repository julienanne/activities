class ClientsController < ApplicationController
  def index
    @clients = Client.all
    json_response(@clients)
  end
  
  def show
    client = get_client
    json_response(client)
  end

  def create
    client = Client.create!(client_params)
    json_response(client, :created)
  end
  
  def update
    client = get_client
    client.update!(client_params)
    json_response(client, :accepted)
  end

  def destroy
    client = get_client
    client.destroy
    head :no_content
  end
  
  private
    def client_params
      params.require(:client).permit(:name)
    end

    def get_client
      Client.find(params[:id])
    end
end
