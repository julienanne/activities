export const callRetry = (authorization, request, callbackAuthentification, callbackSuccess, callbackError, callbackServerError) => {
  if (authorization === null) {    
    callbackAuthentification(null)
  } else {
    let authHeaders = request.headers

    authHeaders.append("Authorization", authorization)
    let authInit = { 
      headers: authHeaders 
    }
    let authRequest = new Request(request, authInit)
    fetch(authRequest)
      .then(
        (result) => {
          if ([200, 201].includes(result.status)){
            result.json().then(
              (res) => {
                // console.log(res)
                // if (component !== null) {
                //   component.setState({
                //     fetching: false,
                //     clients: res,
                //     error: null
                //   })
                // }
                callbackSuccess(res)
              }
            )
          } else if (result.status === 401) {
            callbackAuthentification(null)
            // if (component !== null) {
              // component.setState({
              //   fetching: false,
              //   error: {"message":"Connectez-vous"}
              // })
            // }
          } else {
            callbackServerError()
            // console.log(result)
            // if (component !== null) {
            //   component.setState({
            //     fetching: false,
            //     error: {"message":"Problème serveur"}
            //   })
            // }            
          }
        },
        // Remarque : il est important de traiter les erreurs ici
        // au lieu d'utiliser un bloc catch(), pour ne pas passer à la trappe
        // des exceptions provenant de réels bugs du composant.
        (error) => {
          callbackError(error)
          // if (component !== null) {
          //   component.setState({
          //     fetching: false,
          //     error
          //   })
          // }
          // console.log(error)
        }
      )
  } 
}

// const connect = (component) => {
//   callbackAuthentification
// }