import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import AddClient from '../components/AddClient';

it('renders component with a form', () => {
  const { getByRole } = render(<BrowserRouter initialEntries={["/add-client"]}><AddClient /></BrowserRouter>)
  expect(getByRole("form")).toBeInTheDocument()
  expect(getByRole("button")).toBeInTheDocument()
});