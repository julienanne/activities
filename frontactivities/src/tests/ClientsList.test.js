import { render } from '@testing-library/react';
import { BrowserRouter, Route } from 'react-router-dom';
import ClientsList from '../components/ClientsList';

beforeEach(() => {
  fetch.resetMocks();
  fetch.mockResponseOnce(JSON.stringify([{id: 1, name: 'Aname'}]));
});

it('renders component title by text', async () => {  
  const { findByText } = render(<BrowserRouter initialEntries={["/"]}><Route path="/" render={(props) => <ClientsList {...props} authorization={"Un token"} callbackAuthentification={() => {}} /> } /></BrowserRouter>)
  expect(await findByText(/liste des clients/i)).toBeInTheDocument()
});

it('renders component title', async () => {  
  const { findByRole } = render(<BrowserRouter initialEntries={["/"]}><Route path="/" render={(props) => <ClientsList {...props} authorization={"Un token"} callbackAuthentification={() => {}} /> } /></BrowserRouter>)
  const element = await findByRole('heading')
  expect(element).toBeInTheDocument()
  expect(element).toHaveTextContent(/liste des clients/i)
});

it('renders component table', async () => {
  const { findByRole } = render(<BrowserRouter initialEntries={["/"]}><Route path="/" render={(props) => <ClientsList {...props} authorization={"Un token"} callbackAuthentification={() => {}} /> } /></BrowserRouter>)
  expect(await findByRole('table')).toBeInTheDocument()
});

it('renders component thead for the table', async () => {
  const { findAllByRole } = render(<BrowserRouter initialEntries={["/"]}><Route path="/" render={(props) => <ClientsList {...props} authorization={"Un token"} callbackAuthentification={() => {}} /> } /></BrowserRouter>)
  const elements = await findAllByRole('rowgroup')
  expect(elements[0]).toBeInTheDocument()
});

it('renders component tr for the thead table', async () => {
  const { findAllByRole } = render(<BrowserRouter initialEntries={["/"]}><Route path="/" render={(props) => <ClientsList {...props} authorization={"Un token"} callbackAuthentification={() => {}} /> } /></BrowserRouter>)
  const elements = await findAllByRole('row')
  expect(elements[0]).toBeInTheDocument()
});

it('renders component th nom for the table', async () => {
  const { findAllByRole } = render(<BrowserRouter initialEntries={["/"]}><Route path="/" render={(props) => <ClientsList {...props} authorization={"Un token"} callbackAuthentification={() => {}} /> } /></BrowserRouter>)
  const elements = await findAllByRole('columnheader')
  expect(elements[1]).toBeInTheDocument()
  expect(elements[1]).toHaveTextContent(/nom/i)
});

it('renders component th actions for the table', async () => {
  const { findAllByRole } = render(<BrowserRouter initialEntries={["/"]}><Route path="/" render={(props) => <ClientsList {...props} authorization={"Un token"} callbackAuthentification={() => {}} /> } /></BrowserRouter>)
  const elements = await findAllByRole('columnheader')
  expect(elements[2]).toBeInTheDocument()
  expect(elements[2]).toHaveTextContent(/actions/i)
});

it('renders component tbody for the table', async () => {
  const { findAllByRole } = render(<BrowserRouter initialEntries={["/"]}><Route path="/" render={(props) => <ClientsList {...props} authorization={"Un token"} callbackAuthentification={() => {}} /> } /></BrowserRouter>)
  const elements = await findAllByRole('rowgroup')
  expect(elements[1]).toBeInTheDocument()
});

it('renders component tr for the tbody table', async () => {
  const { findAllByRole, findByText } = render(<BrowserRouter initialEntries={["/"]}><Route path="/" render={(props) => <ClientsList {...props} authorization={"Un token"} callbackAuthentification={() => {}} /> } /></BrowserRouter>)
  const elements = await findAllByRole('row')
  expect(elements[1]).toBeInTheDocument()
  expect(await findByText('Aname')).toBeInTheDocument()
});

it('renders component many td for the table', async () => {
  const { findAllByRole } = render(<BrowserRouter initialEntries={["/"]}><Route path="/" render={(props) => <ClientsList {...props} authorization={"Un token"} callbackAuthentification={() => {}} /> } /></BrowserRouter>)
  const elements = await findAllByRole('cell')
  expect(elements.length).toBeGreaterThan(0)
});

it('renders component link add client', async () => {
  const { findByRole } = render(<BrowserRouter initialEntries={["/"]}><Route path="/" render={(props) => <ClientsList {...props} authorization={"Un token"} callbackAuthentification={() => {}} /> } /></BrowserRouter>)
  expect(await findByRole('link')).toBeInTheDocument()
});