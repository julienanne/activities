import { render } from '@testing-library/react';
import Client from '../components/Client';

it('renders component name and id', () => {
  const { getByText } = render(<table><tbody><Client name="Aname" id={100} key={100} /></tbody></table>)
  expect(getByText(/aname/i)).toBeInTheDocument()
  expect(getByText(/100/i)).toBeInTheDocument()
});