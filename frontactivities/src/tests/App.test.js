import React from 'react';

import { render } from '@testing-library/react'
import { create, act } from 'react-test-renderer';
import { BrowserRouter } from 'react-router-dom';
import App from '../components/App'
import AddClient from '../components/AddClient';
import ClientsList from '../components/ClientsList';
import Authentification from '../components/Authentification';

beforeEach(() => {
  fetch.resetMocks();
  fetch.mockResponseOnce(JSON.stringify([{id: 1, name: 'Aname'}]));
});

it('renders authentification page', () => {
  // const { getAllByRole } = render(<BrowserRouter initialEntries={["/"]}><App /></BrowserRouter>)
  // const linkElement = getAllByRole('heading')[0]
  // expect(linkElement).toBeInTheDocument()
  // expect(linkElement).toHaveTextContent(/activités/i)
  let renderRoot
  act(() => {
    renderRoot = create(<React.StrictMode><BrowserRouter><App /></BrowserRouter></React.StrictMode>)
  })
  
  const authentificationComponent = renderRoot.root.findByType(App).findAllByType(Authentification)
  expect(authentificationComponent).toHaveLength(1)
});


// Je n'arrive pas à placer un faux token dans le state de App
it('renders client list', () => {
  let renderRoot
  act(() => {
    renderRoot = create(<React.StrictMode><BrowserRouter><App /></BrowserRouter></React.StrictMode>)
  })

  let appComponent = renderRoot.root.findByType(App).instance
  appComponent.setState({
    authorization: "Atoken"
  })

  act(() => {
    renderRoot.update(<React.StrictMode><BrowserRouter><App /></BrowserRouter></React.StrictMode>)
  })

  const clientsListComponent = renderRoot.root.findByType(App).findAllByType(ClientsList)
  expect(clientsListComponent).toHaveLength(1)

  // const { findAllByText, getAllByRole } = render(<BrowserRouter initialEntries={["/"]}><App /></BrowserRouter>)  
  // await findAllByText(/actions/i)
  // const linkElement = getAllByRole('heading')[1]
  // expect(linkElement).toHaveTextContent(/liste des clients/i)
});

it('renders add client', () => {
  // const myComponent = <App />
  // myComponent.setState({
  //   authorization: "A token"
  // })
  let renderRoot
  act(() => {
    renderRoot = create(<React.StrictMode><BrowserRouter><App /></BrowserRouter></React.StrictMode>)
  })
  renderRoot.root.findByType(BrowserRouter).instance.history.push('/add-client')


  // console.log(renderRoot.root.findByType(App).children[0].children[1].children)

  let appComponent = renderRoot.root.findByType(App).instance
  // console.log(myComponent.getInstance())
  // console.log(appComponent)
  // console.log(myComponent.root.children[0].children[0])
  // console.log(myComponent.root.findByType(App).state)
  appComponent.setState({
    authorization: "Atoken"
  })
  // console.log(appComponent.state)
  // console.log(appComponent)
  act(() => {
    renderRoot.update(<React.StrictMode><BrowserRouter><App /></BrowserRouter></React.StrictMode>)
  })

  // console.log(renderRoot.root.findByType(App).children[0].children[1].children[0].children[0].children)
  

  const addClientComponent = renderRoot.root.findByType(App).findAllByType(AddClient)

  // console.log(renderRoot.root.findByType(App).children[0].children[1].children[0].children[0].children)

  expect(addClientComponent).toHaveLength(1)
});

