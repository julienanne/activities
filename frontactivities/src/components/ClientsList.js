import React from 'react'
import { NavLink } from 'react-router-dom'
import { FaPlusSquare } from 'react-icons/fa'

import Fetching from './Fetching';
import Client from './Client';
import { callRetry } from '../helpers';

class ClientsList extends React.Component {
  state = {
    error: null,
    clients: [],
    fetching: true
  }

  componentDidMount = () => {        
    // fetch("http://localhost:3000/clients")
    //   .then(
    //     (result) => {
    //       if (result.status === 200){
    //         this.setState({
    //           fetching: false,
    //           clients: result.json(),
    //           error: null
    //         })
    //       } else if (result.status === 401) {
    //         this.setState({
    //           fetching: false,
    //           clients: [],
    //           error: {"message":"Connectez-vous"}
    //         })
    //       } else {
    //         console.log(result)
    //         this.setState({
    //           fetching: false,
    //           clients: [],
    //           error: {"message":"Problème serveur"}
    //         })
    //       }
    //     },
    //     // Remarque : il est important de traiter les erreurs ici
    //     // au lieu d'utiliser un bloc catch(), pour ne pas passer à la trappe
    //     // des exceptions provenant de réels bugs du composant.
    //     (error) => {
    //       this.setState({
    //         fetching: false,
    //         clients: [],
    //         error
    //       })
    //       console.log(error)
    //     }
    //   )

    let init = { 
      method: 'GET',
      headers: new Headers(),
      mode: 'cors',
      cache: 'default' 
    }
    let request = new Request('http://192.168.1.129:3000/clients', init)

    callRetry(
      this.props.authorization, 
      request, 
      this.props.callbackAuthentification, 
      (res) => {
        this.setState({
          fetching: false,
          clients: res,
          error: null
        })
      },
      (error) => {
        this.setState({
          fetching: false,
          error
        })
      },
      () => {
        this.setState({
          fetching: false,
          error: {"message":"Problème serveur"}
        })
      }
    )


    // let delay = Math.floor(Math.random() * 5000)

    // setTimeout(() => {
    //   this.setState({
    //     fetching: false,
    //     clients: [{id: 100, name: "toto"}]
    //   })
    // }, delay)
    
  }


  render() {
    const { error, fetching, clients } = this.state
    const messageSuccess = this.props.location.state ? this.props.location.state.messageSuccess : null

    if (fetching === true) {
      return (<Fetching />)
    }
    else if (error) {
      return (<div>Erreur : {error.message}</div>)
    } 
    else {
      return (
        <div className="clients-list">
          <h2>Liste des clients</h2>
          {messageSuccess && 
            <div className="text-success">{messageSuccess}</div>
          }
          <NavLink to="/add-client" className="btn btn-outline-dark bg-light" exact={true}>Ajouter un client <FaPlusSquare /></NavLink>
          <table className="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Nom</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {clients.map((client) => (<Client name={client.name} key={client.id} id={client.id} />))}
            </tbody>
          </table>
        </div>
      )
    }
  }
}

export default ClientsList;
