// import '../css/Fetching.css';

import React from 'react'
// import { FaSpinner } from 'react-icons/fa'

class Authentification extends React.Component {
  state = {
    error: null
  }

  handleSubmit = (e) => {
    e.preventDefault()
    fetch("http://192.168.1.129:3000/users/sign_in", {
      headers: {
        'Content-type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify({ user: { email: this.inputEmail.value, password: this.inputPassword.value } })
    }).then(
      (result) => {
        let authorization = result.headers.get("Authorization")
        if (authorization !== null){
          this.props.changeAuthorization(authorization)
        } else {
          this.setState({
            error: { "message": "Login ou mot de passe incorrect" }
          })
        }
      }
    )
  }

  render() {
    return (
      <div className="authentification card mx-3">
        {this.state.error &&
          <div className="text-danger">Erreur : {this.state.error.message}</div>
        }
        <div>Veuillez vous identifier</div>
        <form className="card-body" onSubmit={(e) => this.handleSubmit(e)}>
          <div className="mb-2">
            <label form="email">Email</label>
            <input type="text" className="form-control" name="email" id="email" required ref={input => this.inputEmail = input} />
          </div>
          <div className="mb-2">
            <label form="password">Mot de passe</label>
            <input type="password" className="form-control" name="password" id="password" required ref={input => this.inputPassword = input} />
          </div>
          <button type="submit" className="btn btn-primary">Se connecter</button>
        </form>
      </div>
    )
  }
} 

export default Authentification