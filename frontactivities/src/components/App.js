import '../css/App.css';

import React from 'react'
import {Switch, Route} from 'react-router-dom'

import ClientsList from './ClientsList';
import AddClient from './AddClient';
import Authentification from './Authentification';

class App extends React.Component {

  state = {
    authorization: null
  }

  handleChangeAuthorization = (authorization) => {
    this.setState({
      authorization: authorization
    })
  }

  render() {            
    return (
      <div className="App">
        <header className="App-header">
          <h1>Activités</h1>
        </header>
        <div className="App-content">
          {this.state.authorization === null 
          ? <Authentification changeAuthorization={this.handleChangeAuthorization} /> 
          : (
            <Switch>
              <Route path="/add-client" render={(props) => <AddClient {...props} authorization={this.state.authorization} callbackAuthentification={this.handleChangeAuthorization} />} />
              <Route path="/" render={(props) => <ClientsList {...props} authorization={this.state.authorization} callbackAuthentification={this.handleChangeAuthorization} /> } />
            </Switch>
            )
          }
        </div>
      </div>
    )
  }
}

export default App;
