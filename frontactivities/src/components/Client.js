import React from 'react'

class Client extends React.Component {
  render() {
    return (
      <tr>
        <td>
          {this.props.id}
        </td>
        <td>
          {this.props.name}
        </td>
        <td>Actions</td>
      </tr>
    )
  }
}

export default Client;
