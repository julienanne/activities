import React from 'react'
import { NavLink } from 'react-router-dom';
import { callRetry } from '../helpers';
import Fetching from './Fetching';

class AddClient extends React.Component {
  state = {
    error: null,
    fetching: false
  }

  handleRedirect

  handleSubmit = (e) => {
    this.setState({
      fetching: true
    })
    e.preventDefault()
    const data = new FormData()
    data.append("client[name]", this.newClient.value);

    let init = { 
      method: 'POST',
      headers: new Headers({'Content-Type': 'application/json'}),
      mode: 'cors',
      cache: 'default',
      // body: JSON.stringify({ client: { name: "Aneaaa" } })
      // body: data
      body: JSON.stringify({ client: { name: this.newClient.value } })
    }
    let request = new Request('http://192.168.1.129:3000/clients', init)

    callRetry(
      this.props.authorization, 
      request, 
      this.props.callbackAuthentification, 
      (res) => {
        this.setState({
          error: null,
          fetching: false
        })
        this.props.history.push('/', { messageSuccess: "Client ajouté avec succès" })
      },
      (error) => {
        this.setState({
          fetching: false,
          error
        })
      },
      () => {
        this.setState({
          fetching: false,
          error: {"message":"Problème serveur"}
        })
      }
    )

    // let init = { 
    //   method: 'POST',
    //   headers: new Headers({"Content-Type": "application/json"}),
    //   mode: 'cors',
    //   cache: 'default',
    //   body: JSON.stringify({ client: { name: this.newClient.value } })
    // }
    // let request = new Request('http://192.168.1.129:3000/clients', init)

    // callRetry(this, this.props.authorization, request, this.props.callbackAuthentification)

    
    // fetch("http://localhost:3000/clients", {
    //   method: 'POST',
    //   body: data
    // })
    // ICI faire la gestion du message de succès ou d'erreur suite à l'ajout Julien

      // .then(res => res.json())
      // .then(
      //   (result) => {
      //     this.setState({
      //       fetching: false,
      //       clients: result,
      //       error: null
      //     })
      //   },
      //   // Remarque : il est important de traiter les erreurs ici
      //   // au lieu d'utiliser un bloc catch(), pour ne pas passer à la trappe
      //   // des exceptions provenant de réels bugs du composant.
      //   (error) => {
      //     this.setState({
      //       fetching: false,
      //       error
      //     })
      //   }
      // )
  }

  render() {
    const { error, fetching } = this.state;

    if (fetching === true) {
      return (<Fetching />)
    }     
    else {
      return (
        <div className="add-client">
          <h2>Ajouter un client</h2>
          {error !== null &&
            <div className="text-danger">Erreur : {error.message}</div>
          }
          <NavLink to="/" className="btn btn-secondary mb-2" exact={true}>Retour</NavLink>
          <div className="card mx-3">
            <form name="add-client" className="card-body" onSubmit={(e) => this.handleSubmit(e)}>          
              <div className="mb-2">
                <label form="clientName">Nom du client</label>
                <input type="text" className="form-control" name="clientName" id="clientName" required ref={input => this.newClient = input} />
              </div>
              <button type="submit" className="btn btn-primary">Ajouter</button>
            </form>
          </div>
        </div>
      )
      }
  }
}

export default AddClient;
